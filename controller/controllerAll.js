module.exports = {
    allFirstEndPoint(req, res, next) {
        const reqMethod = req.method
        const reqUrl = req.url
        console.log("End point called: " + reqMethod + " " + reqUrl)
        next()
    },
    allLastEndPoint(req, res){
        console.log("Catch-all endpoint aangeroepen")
        const error = {
          message: "Endpoint does not exist"
        }
        res.status(400).json(error).end()
    }


}