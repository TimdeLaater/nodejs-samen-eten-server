const timeToWait = 1500
const logger = require('tracer').console()
const database = require("../dao/database")
const assert = require('assert')
const { dbconfig } = require('../dao/configDatabase')


module.exports = {
    validateNewMeal(req, res, next) {
        logger.log("Validate aangeroepen")
        try {
            const naam = req.body.naam
            logger.log(naam)
            const beschrijving = req.body.beschrijving
            const ingredienten = req.body.ingredienten
            const allergien = req.body.allergien
            const aangebodenOp = req.body.aangebodenOp
            const prijs = req.body.prijs

            const studenthomeId = req.params['id']
            const aantalPersonen = req.body.aantalPersonen

            assert(typeof naam === "string", "naam is missing")
            assert(typeof beschrijving === "string", "beschrijving is missing")
            assert(typeof ingredienten === "string", "ingredienten is missing")
            assert(typeof allergien === "string", "allergien is missing")
            assert(typeof aangebodenOp === "string", "aangebodenOp is missing")
            assert(typeof prijs === "string", "prijs is missing")
            assert(typeof studenthomeId === "string", "StudenthomeId is missing")
            assert(typeof aantalPersonen === "string", "aantalpersonen is missing")

            assert.match(aangebodenOp, /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
            logger.log("validate new meal is accepted")

            next()
        } catch (err) {
            logger.log("Invalid data", err.message)
            res.status(412).send({
                Message: "Een of meer properties in de request body ontbreken of zijn foutief",
            })
        }
    },

    postNewMeal(req, res, next) {
        logger.log("post new meal aangeroepen")
        var date = new Date()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var year = date.getFullYear()

        const naam = req.body.naam
        const beschrijving = req.body.beschrijving
        const ingredienten = req.body.ingredienten
        const allergien = req.body.allergien
        const gemaaktOp = year + "-" + month + "-" + day
        const aangebodenOp = req.body.aangebodenOp
        const prijs = req.body.prijs
        const userid = req.userId
        const studenthomeId = req.params['id']
        const aantalPersonen = req.body.aantalPersonen

        const query = {
            sql: "INSERT INTO meal (Name,Description, Ingredients,Allergies,CreatedOn,OfferedOn,Price,UserID,StudenthomeID,MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            values: [naam, beschrijving, ingredienten, allergien, gemaaktOp, aangebodenOp, prijs, userid, studenthomeId, aantalPersonen],
            timeout: timeToWait
        }

        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(500).json(error.toString())
            } else {
                res.status(200).send({
                    Message: "Meal is created"
                })
            }
        })
    },

    updateMeal(req, res, next) {
        logger.log("Update meal aangeroepen")
        const id = req.params["id"]
        const mealID = req.params["mealId"]
        var date = new Date()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var year = date.getFullYear()

        const naam = req.body.naam
        const beschrijving = req.body.beschrijving
        const ingredienten = req.body.ingredienten
        const allergien = req.body.allergien
        const gemaaktOp = year + "-" + month + "-" + day
        const aangebodenOp = req.body.aangebodenOp
        const prijs = req.body.prijs
        const userid = req.userId
        const studenthomeId = req.params['id']
        const aantalPersonen = req.body.aantalPersonen

        if (id && mealID) {
            logger.log(id, mealID)
            const query = {

                sql: "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ? AND UserID = ?",

                values: [mealID, id, userid],

                timeout: timeToWait

            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString())
                } else if (rows != 0) {
                    logger.log("Has a meal existing")
                    const query = {
                        sql: "UPDATE meal SET Name = ?, Description =?, Ingredients = ? ,Allergies =?, CreatedOn = ?, OfferedOn = ?, Price = ?,UserID = ?,StudenthomeID = ?, MaxParticipants = ?",

                        values: [naam, beschrijving, ingredienten, allergien, gemaaktOp, aangebodenOp, prijs, userid, studenthomeId, aantalPersonen],

                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(500).json(error.toString())
                        } else {
                            res.status(200).send({message: "Meal: " + mealID + " Updated succesfuly"})
                        }
                    })
                }
                else {
                    res.status(401).send({message:"You do not have permission to update this meal"})
                }
            })

        } else {
            res.status(400).send({
                Message: "Invalid houseId and invalid mealId or missing id"
            });


        }
    },

    getMeals(req, res, next) {
        logger.log("getMeals aangeroepen")
        const studenthomeID = req.params["id"]

        const query = {
            sql: "SELECT ID, Name, Description, OfferedOn, Price, MaxParticipants FROM meal WHERE StudenthomeID = ?",
            values: [studenthomeID],
            timeout: timeToWait
        }
        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(500).json(error.toString())
            } else {
                res.status(200).send({message:rows})
            }
        })
    },

    getMealInformation(req, res, next) {
        logger.log("getMealInformation aangeroepen")
        const studenthomeID = req.params["id"]
        const mealID = req.params["mealID"]

        const query = {
            sql: "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ?",
            values: [mealID, studenthomeID],
            timeout: timeToWait
        }
        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(500).json(error.toString())
            }if(rows != 0){
                res.status(200).send({message:rows})
            }
             else {
                res.status(404).send({message:"Meal does not exist"})
            }
        })
    },


    deleteMeal(req, res, next) {
        logger.log("deleteMeal aangeroepen")

        const mealId = req.params["mealID"]

        const userId = req.userId



        const querySelect = {

            sql: "SELECT * FROM meal WHERE ID = ? && UserID = ?",

            values: [mealId, userId],

            timeout: timeToWait

        }



        const query = {

            sql: "DELETE FROM meal WHERE ID = ? && UserID = ?",

            values: [mealId, userId],

            timeout: timeToWait

        }



        database.query(querySelect, (error, rows, fields) => {

            if (error) {

                res.status(500).json(error.toString())

            } else if (rows != 0) {
                database.query(query, (error, rows, fields) => {

                    logger.log(rows)

                    if (error) {
                        res.status(500).json(error.toString())
                    } else {
                        res.status(200).json({
                            Message: "Verwijderd ",
                            rows
                        })
                    }
                })
            }else {
                res.status(401).send({message: "You dont have the autorisation to delete this meal"})
            }
        })
    }
}

