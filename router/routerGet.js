var express = require('express')
var logger = require('tracer').console()
var router = express.Router()
const controllerGet = require("../controller/controllerGet")

router.get('/', (req, res) => {
  controllerGet.getInfoFunction(req,res)
})

router.get('/info', (req, res) => {
  controllerGet.anotherGetInfo(req,res)
})

module.exports = router