var express = require('express')
const controllerParticipants = require('../controller/controllerParticipants')
const auth = require('../controller/controllerAuthentication')
var router = express.Router()

router.post('/studenthome/:homeId/meal/:mealId/signup',auth.validateToken,
controllerParticipants.validateSignup, controllerParticipants.signupForMeal
)
router.put('/studenthome/:homeId/meal/:mealId/signoff',auth.validateToken,controllerParticipants.signoffForMeal)
router.get('/meal/:mealId/participants',auth.validateToken, controllerParticipants.getParticipants)
router.get('/meal/:mealId/participants/:participantId',auth.validateToken, controllerParticipants.getDetailsParticipants)

module.exports = router