//
// Authentication routes
//

const routes = require('express').Router()
const AuthController = require('../controller/controllerAuthentication')

routes.post('/login', AuthController.validateLogin, AuthController.login)
routes.post(
  '/register',
  AuthController.validateRegister,
  AuthController.validateEmail,
  AuthController.register,
)
routes.get('/validate', AuthController.validateToken, AuthController.renewToken)

module.exports = routes
