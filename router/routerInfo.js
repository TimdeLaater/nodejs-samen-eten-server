const controllerAll = require("../controller/controllerInfo")
const router = require('express').Router()

router.get('/', (req, res) => {
    controllerAll.getInfoFunction(req, res)
})

module.exports = router