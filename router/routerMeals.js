var express = require('express')
const controllerMeals = require('../controller/controllerMeals')
const auth = require('../controller/controllerAuthentication')
var router = express.Router()

router.post('/:id/meal',auth.validateToken,
    controllerMeals.validateNewMeal,
    controllerMeals.postNewMeal
)
router.put('/:id/meal/:mealId',auth.validateToken, controllerMeals.validateNewMeal,controllerMeals.updateMeal)
router.get('/:id/meal', controllerMeals.getMeals)
router.get('/:id/meal/:mealID', controllerMeals.getMealInformation)
router.delete('/:id/meal/:mealID',auth.validateToken, controllerMeals.deleteMeal)

module.exports = router