var express = require('express')
const controllerStudentHome = require("../controller/controllerStudentHome")
const controllerMeals = require('../controller/controllerMeals')
const auth = require('../controller/controllerAuthentication')
var router = express.Router()

router.post('/',
    auth.validateToken,
    controllerStudentHome.validatePostalCodeAndCity, controllerStudentHome.validateUniquePostalCodeAdress,
    controllerStudentHome.postNewStudentHome
)
router.get('/', controllerStudentHome.getStudentHomes)
router.get('/', controllerStudentHome.getStudentHomeByName)
router.get('/', controllerStudentHome.getStudentHomeByPlace)
router.get('/:id', controllerStudentHome.getStudentHomeById)
router.put('/:id', auth.validateToken, controllerStudentHome.validatePostalCodeAndCity, controllerStudentHome.validateUniquePostalCodeAdress,
    controllerStudentHome.updateStudentHome
)
router.delete('/:id', auth.validateToken, controllerStudentHome.deleteStudentHomeByPlace)

module.exports = router