const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../server")
const jwt = require("jsonwebtoken")
const assert = require("assert")
const pool = require("../dao/database")
const logger = require('tracer').console()
chai.should()
chai.use(chaiHttp)

describe("StudentHome", function () {
	// 201

	describe("create", function () {
		it("TC-201-1 should return a 400 when field misssing", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					address: "Bastionstaat",
					houseNr: "13b",
					postalCode: "TEST-POSTAL-1",
					phoneNumber: "0681171454",
					cityName: "Breda",
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-201-2 invalid postalcode", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					houseName: "Mooi Huis",
					address: "Bastionstaat",
					houseNr: "13b",
					postalCode: 1,
					phoneNumber: "0681171454",
					cityName: "Breda",
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-201-3 invalid telephone number", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					houseName: "Mooi Huis",
					address: "Bastionstaat",
					houseNr: "13b",
					postalCode: "TEST-POSTAL-1",
					phoneNumber: 1,
					cityName: "Breda",
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a("object")
					done()
				})
		})
	})

		describe("create", function () {
			it("TC-201-4 studenthome already exists", (done) => {
				chai.request(server)
					.post("/api/studenthome/")
					.set(
						"authorization",
						"Bearer " + jwt.sign({ id: 1 }, "secret")
					)
					.send({
						houseName: "TEST_NAME Huis",
						address: "TEST_ADRESS",
						houseNr: "1",
						postalCode: "TEST_POSTCODE1",
						phoneNumber: "0681171454",
						cityName: "Breda",
					})
					.end((err, res) => {
						res.should.have.status(400)
						res.body.should.be.a("object")
						done()
					})
			})
		})

	describe("create", function () {
		it("TC-201-5 not logged in", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.send({
					naam: "Mooi Huis",
					straatnaam: "Bastionstaat",
					huisnummer: "13b",
					postcode: "TEST-POSTAL-1",
					telefoonnummer: 1,
					plaats: "Breda",
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-201-6 should return a 200 message when done correctly", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					naam: "Het A4 huis",
					straatnaam: "Straaat",
					huisnummer: "420",
					postcode: "1224AB",
					plaats: "Zundert",
					telefoonnummer: "061234567891"
				})
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a("object")
					done()
				})
		})
	})
	// 202
	describe("get", function () {
		it("TC-202-2 show 2 different studenthomes", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({plaats: "Breda" })
				.end((err, res) => {
					res.should.have.status(200)

					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("get", function () {
		it("TC-202-3 show 404 error message after non existant city", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ name: "wrong", city: "wrong" })
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("get", function () {
		it("TC-202-4 show 404 error message after non existant name", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ name: "wrong", city: "wrong" })
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("get", function () {
		it("TC-202-5 show studenthome and return code 200", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ name: "TEST_NAME", plaats: "Breda" })
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a("object")
					done()
				})
		})
	})
	describe("get", function () {
		it("TC-202-6 show studenthome when searched for house and return code 200", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ name: "TEST_NAME", plaats: "Breda" })
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	// 203

	describe("get", function () {
		it("TC-203-1 studenthome id doesn't exist and return 401 code", (done) => {
			chai.request(server)
				.get("/api/studenthome/521")
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("get", function () {
		it("TC-203-2 studenthome is shown and 200 code is returned", (done) => {
			chai.request(server)
				.get("/api/studenthome/2")
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	// 204
	describe("put", function () {
		it("TC-204-1 required field is missing and 400 code is returned", (done) => {
			chai.request(server)
				.put("/api/studenthome/0")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					// cityName is missing
					houseName: "Huis",
					postalCode: "4817",
					phoneNumber: "0681171454",
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-204-2 invalid postal code", (done) => {
			chai.request(server)
				.put("/api/studenthome/0")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName: "Breda",
					houseName: "Huis",
					postalCode: 1,
					phoneNumber: "0681171454",
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-204-3 invalid phone number", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName: "Breda",
					houseName: "Huis",
					postalCode: "4817",
					phoneNumber: 1,
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-204-4 studenthome does not exist", (done) => {
			chai.request(server)
				.put("/api/studenthome/444")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					naam: "Het A4 huis",
					straatnaam: "Straaat",
					huisnummer: "420",
					postcode: "1224AB",
					plaats: "Zundert",
					telefoonnummer: "061234567891"
				})
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-204-5 not logged in", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
				.send({
					cityName: "Breda",
					houseName: "Huis",
					postalCode: "4817",
					phoneNumber: "0681171454",
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-204-6 studenthome updated sucessfully", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					naam: "Het A4 huis",
					straatnaam: "Straaat",
					huisnummer: "4200000",
					postcode: "1224AB",
					plaats: "Zundert",
					telefoonnummer: "061234567891"
				})
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	// 205

	describe("delete", function () {
		it("TC-205-1 studenthome doesnt exist", (done) => {
			chai.request(server)
				.delete("/api/studenthome/6")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("delete", function () {
		it("TC-205-2 not logged in", (done) => {
			chai.request(server)
				.delete("/api/studenthome/5")
				// no set function
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a("object")
					done()
				})
		})
	})
	describe("delete", function () {
		it("TC-205-3 user not authorised to delete", (done) => {
			chai.request(server)
				// 3 is made by user 1
				.delete("/api/studenthome/3")
				.set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a("object")
					done()
				})
		})
	})

	describe("delete", function () {
		it("TC-205-4 home deleted succesfully", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a("object")
					done()
				})
		})
	})
})
