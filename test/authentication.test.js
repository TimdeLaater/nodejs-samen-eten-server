const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../server")
const jwt = require("jsonwebtoken")
const assert = require("assert")
const pool = require("../dao/database")
const logger = require('tracer').console()
chai.should()
chai.use(chaiHttp)

const CLEAR_DB =
	"DELETE IGNORE FROM `studenthome`;" +
	"DELETE IGNORE FROM `user`;" +
	"DELETE IGNORE FROM `meal`"
const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"
const CLEAR_USERS_TABLE = "DELETE FROM `user`;"

const INSERT_MEALTIME =
	"INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES" +
	"('1', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '2', '1', '1', '5')," +
	"('2', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '3', '2', '1', '5')," +
	"('4', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '3', '2', '4', '5')," +
	"('3', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '3', '2', '4', '5');"


const INSERT_STUDENTHOMES =
	"INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES " +
	"(1, 'TEST_NAME', 'TEST_ADRESS', '1', '1', 'TEST_POSTCODE1', '0681171454', 'Breda')," +
	"(2, 'TEST_NAME', 'TEST_ADRESS', '1', '2', 'TEST_POSTCODE2', '0681171454', 'Breda')," +
	"(4, 'TEST_NAME_DIFFERENT', 'TEST_ADRESS_DIFFERENNT', '1', '2', 'TEST_POSTCODE4', '0681171454', 'Tilburg')," +
	"(3, 'OTHER_TEST_NAME', 'TEST_ADRESS', '1', '1', '4726BP', '0681171454', 'Tilburg');"


const INSERT_USER =
	"INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
	'(1, "first", "last", "name@server.nl","1234567", "secret"),' +
	'(2, "first1", "last1", "name@server1.nl","1234567", "secret1");'

const INSERT_PARTICIPANTS = "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES" +

"(1, 1, 2, '2021-05-19')," +

"(2, 1, 2, '2021-05-19');" 

// "(4, 1, 1, '2021-05-19')," +

// "(4, 1, 2, '2021-05-19');"


before((done) => {
	pool.query(CLEAR_DB, (err, rows, fields) => {
		if (err) {
			logger.log("CLEARING")
			logger.error(`before CLEARING tables: ${err}`)
			done(err)
		} else {
			logger.log("CLEARING")
			done()
		}
	})
})
after((done) => {
	pool.query(CLEAR_DB, (err, rows, fields) => {
		if (err) {
			logger.log("CLEARING 2")
			console.log(`after error: ${err}`)
			done(err)
		} else {
			logger.log("CLEARING 2")
			logger.info("After FINISHED")
			done()
		}
	})
})

describe("StudentHome", function () {
	before((done) => {
		pool.query(INSERT_USER, (err, rows, fields) => {
			if (err) {
				logger.error(`before INSERT_USER: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before INSERT_USER done`)
				done()
			}
		})
	})
	before((done) => {
		pool.query(INSERT_STUDENTHOMES, (err, rows, fields) => {
			if (err) {
				logger.error(`before studenthome queries: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before studenthome queries`)
				done()
			}
		})
	})

	before((done) => {
		pool.query(INSERT_MEALTIME, (err, rows, fields) => {
			if (err) {
				logger.error(`before mealtime queries: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before mealtime queries`)
				done()
			}
		})
	})
	before((done) => {
		pool.query(INSERT_PARTICIPANTS, (err, rows, fields) => {
			if (err) {
				logger.error(`before INSERT_PARTICIPANTS: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before INSERT_PARTICIPANTS done`)
				done()
			}
		})
	})
	
	// 101
	describe("create", function () {
		it("TC-101-1 Required field missing", (done) => {
			chai.request(server)
				.post("/api/register")
				.send({
					// First name missing
					lastname: "TEST-LASTNAME-1",
					password: "TEST-PASS-1",
					// email already exists
					email: "TEST-LASTNAME-1-@MAIL.com",
				})
				.end((err, res) => {
					res.should.have.status(422)
					
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-101-2 Invalid email", (done) => {
			chai.request(server)
				.post("/api/register")
				.send({
					firstname: "TEST-FIRSTNAME-1",
					lastname: "TEST-LASTNAME-1",
					password: "short",
					email: 1,
				})
				.end((err, res) => {
					res.should.have.status(422)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-101-3 Invalid password", (done) => {
			chai.request(server)
				.post("/api/register")
				.send({
					firstname: "TEST-FIRSTNAME-1",
					lastname: "TEST-LASTNAME-1",
					// faulty passwod
					password: 1,
					email: "TEST-LASTNAME-1-@MAIL.com",
				})
				.end((err, res) => {
					res.should.have.status(422)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-101-4 User already exists", (done) => {
			chai.request(server)
				.post("/api/register")
				.send({
					firstname: "first",
					lastname: "last",
					password: "password",
					email: "name@server.nl",
				})
				.end((err, res) => {
					res.should.have.status(422)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("create", function () {
		it("TC-101-5 User succesfully created", (done) => {
			chai.request(server)
				.post("/api/register")
				.send({
					firstname: "TEST-FIRSTNAME-1",
					lastname: "TEST-LASTNAME-1",
					password: "TEST-PASS-1",
					email: "TEST-LASTNAME-1-@MAIL.com",
				})

				.end((err, res) => {
					res.should.have.status(200)
					
					res.body.should.be.a('object')
			 		const response = res.body
					response.should.have.property('token').which.is.a('string')

					done()
				})
		})
	})

	// 102
	describe("login", function () {
		it("TC-102-1 Required field missing", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					// invalid
					email: 1,
					password: "TEST-PASS-1",
				})
				.end((err, res) => {
					res.should.have.status(422)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("login", function () {
		it("TC-102-2 Invalid email", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					email: 1,
					password: "TEST-PASS-1",
				})
				.end((err, res) => {
					res.should.have.status(422)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("login", function () {
		it("TC-102-3 Invalid password", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					email: "TEST-LASTNAME-2-@MAIL.com",
					// invalid pass
					password: 1,
				})
				.end((err, res) => {
					res.should.have.status(422)
					done()
				})
		})
	})

	describe("login", function () {
		it("TC-102-4 User does not exist", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					password: "TEST-PASS-1",
					email: "NON-EXISTANT-TEST-LASTNAME-2-@MAIL.com",
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("login", function () {
		it("TC-102-5 User sucessfully logged in", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					password: "secret",
					email: "name@server.nl",
				})
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('object')
					res.body.should.have.property('token').which.is.a('string')
					done()
				})
		})
	})
})
