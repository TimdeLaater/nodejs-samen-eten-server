const chai = require("chai");
const timeToWait = 1500;
const chaiHttp = require("chai-http");
const server = require("../server");
const database = require("../dao/database");
const assert = require("assert");
const logger = require('tracer').console()

const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs");


chai.should();
chai.use(chaiHttp);
const CLEAR_DB =
"DELETE IGNORE FROM `studenthome`;" +

"DELETE IGNORE FROM `user`;" +

"DELETE IGNORE FROM `meal`"

const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"

const CLEAR_USERS_TABLE = "DELETE FROM `user`;"



const INSERT_USER =

"INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
'(1, "first", "last", "name@server.nl","1234567", "secret"),' +
'(2, "first1", "last1", "name@server1.nl","1234567", "secret1");'



before((done) => {

database.query(CLEAR_DB, (err, rows, fields) => {

    if (err) {

        logger.log("CLEARING")

        logger.error(`before CLEARING tables: ${err}`)

        done(err)

    } else {

        logger.log("CLEARING")

        done()

    }

})

})

after((done) => {

database.query(CLEAR_DB, (err, rows, fields) => {

    if (err) {

        logger.log("CLEARING 2")

        console.log(`after error: ${err}`)

        done(err)

    } else {

        logger.log("CLEARING 2")

        logger.info("After FINISHED")

        done()

    }

})

})
describe("Studenthome", function () {

    describe("post", function () {
        it("TC-201-1 Return validation error when values are not present", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .send({
                    straatnaam: "coolestraat",
                    huisnummer: "123",
                    postcode: "8247ZS",

                    telefoonnummer: "0612367124"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-201-2 Return validation error when postalcode is not the correct value", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .send({
                    naam: "test",
                    straatnaam: "coolestraat",
                    huisnummer: "123",
                    postcode: "462ZS",
                    plaats: "Heerle",
                    telefoonnummer: "0612367124"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })



        it("TC-201-3 Return validation error when phonenumber is not the correct value", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .send({
                    naam: "test",
                    straatnaam: "coolestraat",
                    huisnummer: "123",
                    postcode: "4862ZS",
                    plaats: "Heerle",
                    telefoonnummer: "212"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-201-4 Return validation error when input value already exist", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .send({
                    // "(3, 'OTHER_TEST_NAME', 'TEST_ADRESS', '1', '1', '4726BP', '0681171454', 'Tilburg');"


                    naam: "OTHER_TEST_NAME",
                    straatnaam: "TEST_ADRESS",
                    huisnummer: "1",
                    postcode: "4726BP",
                    plaats: "Tilburg",
                    telefoonnummer: "0681171454"

                })
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        //TODO 201-5 moet nog gebeuren
        it("TC-201-6 Succesfull post request", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .send({
                    naam: "TestenOsso",
                    straatnaam: "Princenhagen",
                    huisnummer: "141",
                    postcode: "4225VW",
                    plaats: "Zunderd",
                    telefoonnummer: "061234567891"

                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })

})

