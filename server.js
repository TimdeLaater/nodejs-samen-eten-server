const express = require("express")
const app = express()
const port = process.env.PORT || 3000
var logger = require('tracer').console()
app.use(express.json())
const authroutes = require('./router/routerAuthentication')
const studentHome = require("./router/routerStudentHome")
const meals = require('./router/routerMeals')
const info = require('./router/routerInfo')
const part = require('./router/routerParticipants')


app.use("/api/studenthome", studentHome)
app.use('/api/studenthome', meals)
app.use('/api', part)
app.use('/api', authroutes)
app.use('/api/info', info)

app.get('*', function(req, res){

  res.status(404).json({Error: "Route bestaat niet vul een geldige url in"})

});
app.listen(port, () => {
  logger.log(`example app listening at http://localhost:${port}`)
})

module.exports = app