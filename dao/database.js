const timeToWait = 1500

const mysql = require('mysql')
const logger = require('./configDatabase').logger
const dbconfig = require('./configDatabase').dbconfig
const pool = mysql.createConnection(dbconfig)

pool.on('connection', function (connection) {

  logger.log('Database connection established')

})

pool.on('acquire', function (connection) {

  logger.log('Database connection aquired')

})

pool.on('release', function (connection) {

  logger.log('Database connection released')

})

module.exports = pool


